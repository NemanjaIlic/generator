#ifndef NASLOV_H_INCLUDED
#define NASLOV_H_INCLUDED

#include <string>
#include <iostream>

#include "element.h"

using namespace std;

class Naslov: public Element {
protected:
    string sadrzaj;
    string podnaslov;

public:
    Naslov(string naziv, int identifikator, string sadrzaj, string podnaslov);
    void ispis();
};

#endif // NASLOV_H_INCLUDED
