#ifndef ILUSTRACIJA_H_INCLUDED
#define ILUSTRACIJA_H_INCLUDED

#include <string>
#include <iostream>

#include "element.h"

using namespace std;

class Ilustracija: public Element {
protected:
    int redniBroj;
    string url;
    string opis;
    double sirina;
    double visina;

public:
    Ilustracija(string naziv, int identifikator, int redniBroj, string url, string opis, double sirina, double visina);
    void ispis();
    double povrsina();
    void skaliranje(double faktor);
};

#endif // ILUSTRACIJA_H_INCLUDED
