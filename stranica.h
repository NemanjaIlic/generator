#ifndef STRANICA_H_INCLUDED
#define STRANICA_H_INCLUDED

#include <string>
#include <vector>
#include <iostream>

#include "element.h"

using namespace std;

class Stranica {
protected:
    vector<Element*> elementi;

    int redniBroj;

    string zaglavlje;
    string podnozje;
public:
    Stranica(int redniBroj, string zaglavlje, string podnozje);
    void dodajElement(Element* element);
    void ispis();
};

#endif // STRANICA_H_INCLUDED
