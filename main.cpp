#include "element.h"
#include "naslov.h"
#include "paragraf.h"
#include "ilustracija.h"
#include "stranica.h"

int main()
{
    Stranica stranica = Stranica(1, "Zaglavlje stranice", "Podnozje stranice");

    Element *naslov = new Naslov("naslov", 1, "Sadrzaj naslova", "Sadrzaj podnaslova");
    Element *paragraf = new Paragraf("paragraf", 2, "Sadrzaj paragrafa", 10, true, false);
    Element *ilustracija = new Ilustracija("ilustracija", 3, 1, "URL ilustracije", "Opis ilustracije", 100, 200);

    stranica.dodajElement(naslov);
    stranica.dodajElement(paragraf);
    stranica.dodajElement(ilustracija);

    stranica.ispis();

    return 0;
}
