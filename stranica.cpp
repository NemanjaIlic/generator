#include "stranica.h"

Stranica::Stranica(int redniBroj, string zaglavlje, string podnozje) {
    this->redniBroj = redniBroj;
    this->zaglavlje = zaglavlje;
    this->podnozje = podnozje;
}

void Stranica::dodajElement(Element* element) {
    elementi.push_back(element);
}

void Stranica::ispis() {
    cout << zaglavlje << endl;

    for (int i = 0; i < elementi.size(); i++) {
        elementi[i]->ispis();
    }

    cout << podnozje << endl;
    cout << redniBroj << endl;
}
