#include "element.h"

Element::Element(string naziv, int identifikator) {
    this->naziv = naziv;
    this->identifikator = identifikator;
}

void Element::ispis() {
    cout << "Element " << naziv << ":" << identifikator << endl;
}
