#ifndef ELEMENT_H_INCLUDED
#define ELEMENT_H_INCLUDED

#include <string>
#include <iostream>

using namespace std;

class Element {
protected:
    string naziv;
    int identifikator;

public:
    Element(string naziv, int identifikator);
    virtual void ispis();
};

#endif // ELEMENT_H_INCLUDED
