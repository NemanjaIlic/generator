#include "paragraf.h"

Paragraf::Paragraf(string naziv, int identifikator, string sadrzaj, double velicinaSlova, bool podebljan, bool iskosen):
                   Element(naziv, identifikator) {
    this->sadrzaj = sadrzaj;
    this->velicinaSlova = velicinaSlova;
    this->podebljan = podebljan;
    this->iskosen = iskosen;
}

void Paragraf::ispis() {
    cout << "Element " << naziv << ":" << identifikator;
    cout << " [" << sirina() << "x" << visina();
    cout << " I: " << iskosen << " B: " << podebljan << "]: ";
    cout << sadrzaj << endl;
}

double Paragraf::sirina() {
    return velicinaSlova * sadrzaj.length();
}

double Paragraf::visina() {
    return velicinaSlova * 1.25 * sadrzaj.length() / 80;
}
