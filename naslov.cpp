#include "naslov.h"

Naslov::Naslov(string naziv, int identifikator, string sadrzaj, string podnaslov):
               Element(naziv, identifikator) {
    this->sadrzaj = sadrzaj;
    this->podnaslov = podnaslov;
}

void Naslov::ispis() {
    cout << "Element " << naziv << ":" << identifikator << ": " ;
    cout << sadrzaj << "(" << podnaslov << ")" << endl;
}
