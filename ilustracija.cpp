#include "ilustracija.h"

Ilustracija::Ilustracija(string naziv, int identifikator, int redniBroj, string url, string opis, double sirina, double visina):
                   Element(naziv, identifikator) {
    this->redniBroj = redniBroj;
    this->url = url;
    this->opis = opis;
    this->sirina = sirina;
    this->visina = visina;
}

void Ilustracija::ispis() {
    cout << "Element " << naziv << ":" << identifikator;
    cout << " [" << sirina << "x" << visina;
    cout << " URL: " << url << "]: ";
    cout << redniBroj << ". " << naziv << " - " << opis << endl;
}

double Ilustracija::povrsina() {
    return sirina * visina;
}

void Ilustracija::skaliranje(double faktor) {
    sirina *= faktor;
    visina *= faktor;
}
