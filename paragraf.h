#ifndef PARAGRAF_H_INCLUDED
#define PARAGRAF_H_INCLUDED

#include <string>
#include <iostream>

#include "element.h"

using namespace std;

class Paragraf: public Element {
protected:
    string sadrzaj;
    double velicinaSlova;
    bool podebljan;
    bool iskosen;

public:
    Paragraf(string naziv, int identifikator, string sadrzaj, double velicinaSlova, bool podebljan, bool iskosen);
    void ispis();
    double sirina();
    double visina();
};

#endif // PARAGRAF_H_INCLUDED
